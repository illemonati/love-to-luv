use std::fs::{File, OpenOptions};
use std::io::{prelude::*, BufReader, BufWriter};

fn main() {
    let input: File = File::open("./words_alpha.txt").expect("No words_alpha.txt");
    let reader = BufReader::new(input);
    let output: File = OpenOptions::new()
        .read(false)
        .write(true)
        .create(true)
        .open("outputs.txt").expect("Create output file error");
    let mut writer = BufWriter::new(output);
    for word in reader.lines() {
        let word = word.unwrap();
        if word.len() == 4 {
            let bword = word.as_bytes();
            if bword[1] == b'o' && bword[3] == b'e' {
                let _ = writer.write(format!("{} => {}u{}\n", word ,bword[0] as char, bword[2] as char).as_bytes());
            }
        }
    }
}
